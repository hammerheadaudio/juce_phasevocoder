/*
  ==============================================================================

    DSPModule.h
    Created: 20 Aug 2017 10:42:47pm
    Author:  Emmett Palaima

  ==============================================================================
*/

#ifndef DSPMODULE_H_INCLUDED
#define DSPMODULE_H_INCLUDED

#include "AudioModule.h"

class DSPModule : public AudioModule{
public:
    DSPModule(){};
    virtual ~DSPModule(){};

    virtual float process(float input_){
        return 0;
    };
private:
    JUCE_DECLARE_NON_COPYABLE_WITH_LEAK_DETECTOR (DSPModule)
};




#endif  // DSPMODULE_H_INCLUDED
