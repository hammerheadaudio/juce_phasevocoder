/*
  ==============================================================================

    This file was auto-generated!

    It contains the basic framework code for a JUCE plugin editor.

  ==============================================================================
*/

#include "PluginProcessor.h"
#include "PluginEditor.h"


//==============================================================================
Fft_testAudioProcessorEditor::Fft_testAudioProcessorEditor (Fft_testAudioProcessor& p)
    : AudioProcessorEditor (&p), processor (p)
{
    // Make sure that before the constructor has finished, you've set the
    // editor's size to whatever you need it to be.
    setSize (400, 300);
    addSlider(&blurSlider, Slider::LinearBarVertical, 0.0, 2.0, 0.0, 1.0, " Seconds", processor.blurTime);
    addSlider(&atkSlider, Slider::LinearBarVertical, 0.0, 4.0, 0.0, 1.0, " Seconds", processor.envAtk);
    addSlider(&relSlider, Slider::LinearBarVertical, 0.0, 4.0, 0.0, 1.0, " Seconds", processor.envRel);
    addSlider(&depthSlider, Slider::LinearBarVertical, 0.0, 500, 0.0, 50.0, " Depth", processor.envDepth);
    
}

Fft_testAudioProcessorEditor::~Fft_testAudioProcessorEditor()
{
}

void Fft_testAudioProcessorEditor::addSlider(Slider* slider, Slider::SliderStyle style_, float min_, float max_, float incr_, float midpoint_, const String text_, float intialValue_){
    slider->setSliderStyle (style_);
    slider->setRange(min_, max_, incr_);
    slider->setSkewFactorFromMidPoint(midpoint_);
    slider->setTextBoxStyle (Slider::NoTextBox, false, 90, 0);
    slider->setPopupDisplayEnabled (true, this);
    slider->setTextValueSuffix (text_);
    slider->addListener(this);
    addAndMakeVisible (slider);
    slider->setValue(intialValue_);
}

void Fft_testAudioProcessorEditor::sliderValueChanged (Slider* slider) {
    if(slider == &blurSlider){
        processor.blurTime = blurSlider.getValue();
    }
    else if(slider == &atkSlider){
        processor.envAtk = atkSlider.getValue();
        processor.env->setParam(processor.envAtk, EnvelopeFollower::attackParam);
    }
    else if(slider == &relSlider){
        processor.envRel = relSlider.getValue();
        processor.env->setParam(processor.envRel, EnvelopeFollower::releaseParam);
    }
    else if(slider == &depthSlider){
        processor.envDepth = depthSlider.getValue();
        processor.env->setParam(processor.envDepth, EnvelopeFollower::depthParam);
    }
}

//==============================================================================
void Fft_testAudioProcessorEditor::paint (Graphics& g)
{
    g.fillAll (Colours::white);

    g.setColour (Colours::black);
    g.setFont (15.0f);
}

void Fft_testAudioProcessorEditor::resized()
{
    blurSlider.setBounds(10, 10, 20, getHeight() - 20);
    atkSlider.setBounds(60, 10, 20, getHeight() - 20);
    relSlider.setBounds(110, 10, 20, getHeight() - 20);
    depthSlider.setBounds(160, 10, 20, getHeight() - 20);
    // This is generally where you'll want to lay out the positions of any
    // subcomponents in your editor..
}
