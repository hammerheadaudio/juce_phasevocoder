/*
  ==============================================================================

    This file was auto-generated!

    It contains the basic framework code for a JUCE plugin processor.

  ==============================================================================
*/

#ifndef PLUGINPROCESSOR_H_INCLUDED
#define PLUGINPROCESSOR_H_INCLUDED

#include "../JuceLibraryCode/JuceHeader.h"
#include "EnvelopeFollower.h"
#define FFTSIZE 1024
#define OVERLAP 256
#define FFTMAXDEL 172


//==============================================================================
/**
*/
class Fft_testAudioProcessor  : public AudioProcessor
{
public:
    //==============================================================================
    Fft_testAudioProcessor();
    ~Fft_testAudioProcessor();

    //==============================================================================
    void prepareToPlay (double sampleRate, int samplesPerBlock) override;
    void releaseResources() override;

   #ifndef JucePlugin_PreferredChannelConfigurations
    bool isBusesLayoutSupported (const BusesLayout& layouts) const override;
   #endif

    void processBlock (AudioSampleBuffer&, MidiBuffer&) override;

    //==============================================================================
    AudioProcessorEditor* createEditor() override;
    bool hasEditor() const override;

    //==============================================================================
    const String getName() const override;

    bool acceptsMidi() const override;
    bool producesMidi() const override;
    double getTailLengthSeconds() const override;

    //==============================================================================
    int getNumPrograms() override;
    int getCurrentProgram() override;
    void setCurrentProgram (int index) override;
    const String getProgramName (int index) override;
    void changeProgramName (int index, const String& newName) override;

    //==============================================================================
    void getStateInformation (MemoryBlock& destData) override;
    void setStateInformation (const void* data, int sizeInBytes) override;
    
    inline float secondsToFrames(float seconds){
        return (seconds * getSampleRate()) / float(OVERLAP);
    }
    
    void printFFT(float* fft){
        for(int i = 0; i < 4095; i++){
            printf("\n%d: R:%f I: %f", i, fft[i], fft[i+1]);
        }
    }
    
    void generateHannWindow(float* window){
        for(int i = 0; i < FFTSIZE; i++){
            window[i] = .5 * (1 - cos(2.0 * M_PI * i / (FFTSIZE - 1)));
        }
    }
    
    void reverseFFT(FFT::Complex* input, FFT::Complex* output){
        
        //convert forward fft to reverse fft by taking complex conjugate of input
        for(int i = 0; i < FFTSIZE; i++){
            input[i].i *= -1;
        }
        fFFT->perform(input, output);
        for(int i = 0; i < FFTSIZE; i++){
            output[i].r /= float(FFTSIZE);
        }
        
        //alternate method using juce built in reverb fft, sounds the same
        /*
        iFFT->perform(input, output);
        for(int i = 0; i < FFTSIZE; i++){
            output[i].r /= float(FFTSIZE);
        }
        */
    }
    
    typedef struct{
        float amp;
        float freq;
    }PVOC;
    
    void pva(FFT::Complex* inbuf, FFT::Complex* holdbuf, PVOC* outbuf, float* window);
    void pvs(PVOC* inbuf, FFT::Complex* holdbuf, FFT::Complex* outbuf, float* window);
    void processPVOC(PVOC* buffer);
    
    float blurTime;
    float envAtk;
    float envRel;
    float envDepth;
    EnvelopeFollower* env;

private:
    bool startAnalysis = false;
    float feedback;
    int fillIndex;
    int write;
    int read;
    float delay;
    float fracDel;
    int delayFrames;
    float envValue;
    float inbuffer[4096];
    float outbuffer[4096];
    float window[FFTSIZE];
    FFT::Complex fftin[4][FFTSIZE];
    FFT::Complex fftout[4][FFTSIZE];
    FFT::Complex holdfft[FFTSIZE];
    FFT::Complex fftdel[FFTMAXDEL][FFTSIZE];
    PVOC pvoc [4][FFTSIZE];
    PVOC prev [FFTSIZE];
    PVOC pvocDel [FFTMAXDEL][FFTSIZE];
    float lastph[4][FFTSIZE];
    FFT* fFFT;
    FFT* iFFT;
    int pickfft;
    //==============================================================================
    JUCE_DECLARE_NON_COPYABLE_WITH_LEAK_DETECTOR (Fft_testAudioProcessor)
};


#endif  // PLUGINPROCESSOR_H_INCLUDED
