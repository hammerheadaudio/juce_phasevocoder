/*
  ==============================================================================

    EnvelopeFollower.h
    Created: 3 Mar 2017 10:35:16am
    Author:  Emmett Palaima

  ==============================================================================
*/

#ifndef ENVELOPEFOLLOWER_H_INCLUDED
#define ENVELOPEFOLLOWER_H_INCLUDED
#define ENVATTACK 0
#define ENVRELEASE 1
#define ENVDEPTH 2
#define ENVMIN 100
#include "../JuceLibraryCode/JuceHeader.h"
#include "DSPModule.h"

class EnvelopeFollower : public DSPModule{
public:
    EnvelopeFollower(float attack_, float release_, int sampleRate_);
    ~EnvelopeFollower();
       
    void setParam(float newValue_, int param_) override;
    void setSampleRate(int sampleRate_) override;
    float process(float input_) override;
    
    enum params {
        attackParam = 0,
        releaseParam = 1,
        depthParam = 2
    };
    
private:
    float attack;
    float release;
    float atkincr;
    float relincr;
    float abs;
    float depth;
    
    JUCE_DECLARE_NON_COPYABLE_WITH_LEAK_DETECTOR (EnvelopeFollower)
};



#endif  // ENVELOPEFOLLOWER_H_INCLUDED
