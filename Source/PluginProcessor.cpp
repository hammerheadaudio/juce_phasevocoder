/*
  ==============================================================================

    This file was auto-generated!

    It contains the basic framework code for a JUCE plugin processor.

  ==============================================================================
*/

#include "PluginProcessor.h"
#include "PluginEditor.h"


//==============================================================================
Fft_testAudioProcessor::Fft_testAudioProcessor()
#ifndef JucePlugin_PreferredChannelConfigurations
     : AudioProcessor (BusesProperties()
                     #if ! JucePlugin_IsMidiEffect
                      #if ! JucePlugin_IsSynth
                       .withInput  ("Input",  AudioChannelSet::stereo(), true)
                      #endif
                       .withOutput ("Output", AudioChannelSet::stereo(), true)
                     #endif
                       )
#endif
{
    fillIndex = 0;
    write = 0;
    read = 0;
    delayFrames = 10;
    
    blurTime = 0;
    envAtk = 1;
    envRel = 1;
    envDepth = 0;
    
    fFFT = new FFT(log2(FFTSIZE), false);
    iFFT = new FFT(log2(FFTSIZE), true);
    env = new EnvelopeFollower(envAtk, envRel, 44100);
    env->setParam(envDepth, EnvelopeFollower::depthParam);
    memset(lastph, 0, sizeof(float)*FFTSIZE);
    
    generateHannWindow(window);
    
    for(int i = 0; i < FFTSIZE; i++){
        prev[i].amp = prev[i].freq = 0;
        holdfft[i].r = holdfft[i].i = 0;
        for(int j = 0; j < 4; j++){
            fftin[j][i].r = fftin[j][i].i = 0;
            fftout[j][i].r = fftout[j][i].i = 0;
            pvoc[j][i].amp = pvoc[j][i].freq = 0;
            lastph[j][i] = 0;
        }
        for(int j = 0; j < FFTMAXDEL; j++){
            fftdel[j][i].r = fftdel[j][i].i = 0;
            pvocDel[j][i].amp = 0;
            pvocDel[j][i].freq = i * 2 * 44100.0 / float(FFTSIZE);
        }
    }
}

Fft_testAudioProcessor::~Fft_testAudioProcessor()
{
    delete fFFT;
    delete iFFT;
}

//==============================================================================
const String Fft_testAudioProcessor::getName() const
{
    return JucePlugin_Name;
}

bool Fft_testAudioProcessor::acceptsMidi() const
{
   #if JucePlugin_WantsMidiInput
    return true;
   #else
    return false;
   #endif
}

bool Fft_testAudioProcessor::producesMidi() const
{
   #if JucePlugin_ProducesMidiOutput
    return true;
   #else
    return false;
   #endif
}

double Fft_testAudioProcessor::getTailLengthSeconds() const
{
    return 0.0;
}

int Fft_testAudioProcessor::getNumPrograms()
{
    return 1;   // NB: some hosts don't cope very well if you tell them there are 0 programs,
                // so this should be at least 1, even if you're not really implementing programs.
}

int Fft_testAudioProcessor::getCurrentProgram()
{
    return 0;
}

void Fft_testAudioProcessor::setCurrentProgram (int index)
{
}

const String Fft_testAudioProcessor::getProgramName (int index)
{
    return String();
}

void Fft_testAudioProcessor::changeProgramName (int index, const String& newName)
{
}

//==============================================================================
void Fft_testAudioProcessor::prepareToPlay (double sampleRate, int samplesPerBlock)
{
    env->setSampleRate(sampleRate);
    for(int i = 0; i < FFTSIZE; i++){
        for(int j = 0; j < FFTMAXDEL; j++){
            fftdel[j][i].r = fftdel[j][i].i = 0;
            pvocDel[j][i].amp = 0;
            pvocDel[j][i].freq = i * 2 * sampleRate / float(FFTSIZE);
        }
    }
}

void Fft_testAudioProcessor::releaseResources()
{
    // When playback stops, you can use this as an opportunity to free up any
    // spare memory, etc.
}

#ifndef JucePlugin_PreferredChannelConfigurations
bool Fft_testAudioProcessor::isBusesLayoutSupported (const BusesLayout& layouts) const
{
  #if JucePlugin_IsMidiEffect
    ignoreUnused (layouts);
    return true;
  #else
    // This is the place where you check if the layout is supported.
    // In this template code we only support mono or stereo.
    if (layouts.getMainOutputChannelSet() != AudioChannelSet::mono()
     && layouts.getMainOutputChannelSet() != AudioChannelSet::stereo())
        return false;

    // This checks if the input layout matches the output layout
   #if ! JucePlugin_IsSynth
    if (layouts.getMainOutputChannelSet() != layouts.getMainInputChannelSet())
        return false;
   #endif

    return true;
  #endif
}
#endif


void Fft_testAudioProcessor::pva(FFT::Complex* inbuf, FFT::Complex* holdbuf, PVOC* outbuf, float* window){
    //based on Audio Programming Book Example 9.1.1 P.559
    
    float fac = getSampleRate() / (float(OVERLAP) * 2.0 * M_PI);
    float scal = 2.0 * M_PI * float(OVERLAP) / float(FFTSIZE);
    float mag, delta, phi;
    
    for(int j = 0; j < FFTSIZE; j++){
        inbuf[j].r *= window[j];
    }
    
    fFFT->perform(inbuf, holdbuf);
    for(int j = 0; j < FFTSIZE; j++){
        mag = sqrt(pow(holdbuf[j].r, 2) + pow(holdbuf[j].i, 2));
        phi = atan2f(holdbuf[j].i, holdbuf[j].r);
        delta = phi - lastph[0][j];
        lastph[0][j] = phi;
        while(delta > M_PI) delta -= (M_PI * 2.0);
        while(delta < -M_PI) delta += (M_PI * 2.0);
        
        outbuf[j].amp = mag;
        outbuf[j].freq = (delta + (j * scal)) * fac;
    }
}

void Fft_testAudioProcessor::pvs(PVOC* inbuf, FFT::Complex* holdbuf, FFT::Complex* outbuf, float* window){
    //based on Audio Programming Book Example 9.1.1 P.559
    
    float fac = float(OVERLAP) * 2.0 * M_PI / getSampleRate();
    float mag, delta, phi;
    
    float scal = getSampleRate() / float(FFTSIZE);
    for(int j = 0; j < FFTSIZE; j++){
        delta = (inbuf[j].freq - (j * scal)) * fac;
        phi = lastph[1][j] + delta;
        lastph[1][j] = phi;
        mag = inbuf[j].amp;
        
        holdbuf[j].r = mag * cos(phi);
        holdbuf[j].i = mag * sin(phi);
    }
    reverseFFT(holdbuf, outbuf);
    for(int j = 0; j < FFTSIZE; j++){
        outbuf[j].r *= window[j];
    }
}

void Fft_testAudioProcessor::processPVOC(PVOC *input){
    //based on csound pvsblur opcode, csound github /Opcodes/pvsbasic.c lines 2055-2145
    
    float amp, freq;
    int first;
    
    for(int j = 0; j < FFTSIZE; j++){
        amp = freq = 0;
        pvocDel[write][j].amp = input[j].amp;
        pvocDel[write][j].freq = input[j].freq;
        
        if(delayFrames){
            if((first = write - delayFrames) < 0) first += FFTMAXDEL;
            for(read = first; read != write; read = (read + 1) % FFTMAXDEL){
                amp += pvocDel[read][j].amp;
                freq += pvocDel[read][j].freq;
            }
            input[j].amp = amp / float(delayFrames);
            input[j].freq = freq / float(delayFrames);
        }
    }
    
    write = (write + 1) % FFTMAXDEL;
    
}

void Fft_testAudioProcessor::processBlock (AudioSampleBuffer& buffer, MidiBuffer& midiMessages)
{
    const int totalNumInputChannels  = getTotalNumInputChannels();
    const int totalNumOutputChannels = getTotalNumOutputChannels();

    // In case we have more outputs than inputs, this code clears any output
    // channels that didn't contain input data, (because these aren't
    // guaranteed to be empty - they may contain garbage).
    // This is here to avoid people getting screaming feedback
    // when they first compile a plugin, but obviously you don't need to keep
    // this code if your algorithm always overwrites all the output channels.
    for (int i = totalNumInputChannels; i < totalNumOutputChannels; ++i)
        buffer.clear (i, 0, buffer.getNumSamples());

    // This is the place where you'd normally do the guts of your plugin's
    // audio processing...
    float* chan0 = buffer.getWritePointer(0);
    float output;
    
    for (int channel = 0; channel < totalNumInputChannels; ++channel)
    {
        float* channelData = buffer.getWritePointer (channel);
        if(channel == 0){
            //only process first channel
            
            for(int i = 0; i < buffer.getNumSamples(); i++){
                
                //calculate blur time
                envValue = env->process(channelData[i]);
                delayFrames = int(delay = secondsToFrames(blurTime + envValue));
                if(delayFrames >= FFTMAXDEL) delayFrames = FFTMAXDEL - 1;
                fracDel = delay - delayFrames;
                
                
                //fill and read from fft buffers
                output = 0;
                
                if(fillIndex < FFTSIZE){
                    for(int j = 0; j < 4; j++){
                        fftin[j][(fillIndex + (OVERLAP * j)) % FFTSIZE].r = channelData[i];
                        output += fftout[j][(fillIndex + (OVERLAP * j)) % FFTSIZE].r;
                    }
                    channelData[i] = output;
                    fillIndex++;
                }
                
                //process buffers when full
                if((fillIndex % OVERLAP) == 0){
                    
                    //index through different buffers for overlap
                    switch(fillIndex){
                        case FFTSIZE:
                            pickfft = 0;
                            fillIndex = 0; break;
                        case OVERLAP:
                            pickfft = 1; break;
                        case OVERLAP * 2:
                            pickfft = 2; break;
                        case OVERLAP * 3:
                            pickfft = 3; break;
                    }

                    pva(fftin[pickfft], holdfft, pvoc[pickfft], window);    //convert input to pvoc buffer
                    processPVOC(pvoc[pickfft]);                             //process the pvoc buffer
                    pvs(pvoc[pickfft], holdfft, fftout[pickfft], window);   //convert back to time domain signal
                }
            }
        }
        else{
            for(int i = 0; i < buffer.getNumSamples(); i++){
                //copy data from processed channel to other channels for stereo
                channelData[i] = chan0[i];
            }
        }
    }
    //printf("\nDelay Seconds: %f\nDelay: %f\nDelayFrames: %d\nFracDel: %f\n", envValue, delay, delayFrames, fracDel);
}

//==============================================================================
bool Fft_testAudioProcessor::hasEditor() const
{
    return true; // (change this to false if you choose to not supply an editor)
}

AudioProcessorEditor* Fft_testAudioProcessor::createEditor()
{
    return new Fft_testAudioProcessorEditor (*this);
}

//==============================================================================
void Fft_testAudioProcessor::getStateInformation (MemoryBlock& destData)
{
    // You should use this method to store your parameters in the memory block.
    // You could do that either as raw data, or use the XML or ValueTree classes
    // as intermediaries to make it easy to save and load complex data.
}

void Fft_testAudioProcessor::setStateInformation (const void* data, int sizeInBytes)
{
    // You should use this method to restore your parameters from this memory block,
    // whose contents will have been created by the getStateInformation() call.
}

//==============================================================================
// This creates new instances of the plugin..
AudioProcessor* JUCE_CALLTYPE createPluginFilter()
{
    return new Fft_testAudioProcessor();
}
