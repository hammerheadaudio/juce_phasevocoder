/*
  ==============================================================================

    SynthModule.h
    Created: 20 Aug 2017 11:36:50pm
    Author:  Emmett Palaima

  ==============================================================================
*/

#ifndef SYNTHMODULE_H_INCLUDED
#define SYNTHMODULE_H_INCLUDED

#include "AudioModule.h"

class SynthModule : public AudioModule{
public:
    SynthModule(){};
    virtual ~SynthModule(){};
    
    virtual void increment(){};
    virtual float getOutput(){
        return 0;
    };
    JUCE_DECLARE_NON_COPYABLE_WITH_LEAK_DETECTOR (SynthModule)
};



#endif  // SYNTHMODULE_H_INCLUDED
