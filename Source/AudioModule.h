/*
  ==============================================================================

    AudioModule.h
    Created: 16 Aug 2017 9:22:13pm
    Author:  Emmett Palaima

  ==============================================================================
*/

#ifndef AUDIOMODULE_H_INCLUDED
#define AUDIOMODULE_H_INCLUDED

#include "../JuceLibraryCode/JuceHeader.h"

class AudioModule{
public:
    AudioModule(){};
    virtual ~AudioModule(){};
    
    virtual void setParam(float newValue_, int param_){};
    virtual void setSampleRate(int sampleRate_){
        sampleRate = sampleRate_;
    };
    
protected:
    float sampleRate;
    float output = 0;
    JUCE_DECLARE_NON_COPYABLE_WITH_LEAK_DETECTOR (AudioModule)
};

#endif  // AUDIOMODULE_H_INCLUDED
