/*
  ==============================================================================

    This file was auto-generated!

    It contains the basic framework code for a JUCE plugin editor.

  ==============================================================================
*/

#ifndef PLUGINEDITOR_H_INCLUDED
#define PLUGINEDITOR_H_INCLUDED

#include "../JuceLibraryCode/JuceHeader.h"
#include "PluginProcessor.h"


//==============================================================================
/**
*/
class Fft_testAudioProcessorEditor  : public AudioProcessorEditor, private Slider::Listener
{
public:
    Fft_testAudioProcessorEditor (Fft_testAudioProcessor&);
    ~Fft_testAudioProcessorEditor();

    //==============================================================================
    void addSlider(Slider* slider, Slider::SliderStyle style_, float min_, float max_, float incr_, float midpoint_, const String text_, float intialValue_);
    void sliderValueChanged (Slider* slider) override;
    void paint (Graphics&) override;
    void resized() override;

private:
    // This reference is provided as a quick way for your editor to
    // access the processor object that created it.
    Fft_testAudioProcessor& processor;
    Slider blurSlider;
    Slider atkSlider;
    Slider relSlider;
    Slider depthSlider;

    JUCE_DECLARE_NON_COPYABLE_WITH_LEAK_DETECTOR (Fft_testAudioProcessorEditor)
};


#endif  // PLUGINEDITOR_H_INCLUDED
