/*
  ==============================================================================

    EnvelopeFollower.cpp
    Created: 3 Mar 2017 10:35:16am
    Author:  Emmett Palaima

  ==============================================================================
*/

#include "EnvelopeFollower.h"
#include "Math.h"

EnvelopeFollower::EnvelopeFollower(float attack_, float release_, int sampleRate_){
    sampleRate = sampleRate_;
    attack = attack_;
    release = release_;
    if(attack <= 0.0){
        atkincr = expf(-float(69.0775527898)/(sampleRate * attack));
    }
    else{
        atkincr = expf(-float(6.90775527898)/(sampleRate * attack));
    }
    if(release_ <= 0.0){
        relincr = expf(-float(69.0775527898)/(sampleRate * release));
    }
    else{
        relincr = expf(-float(6.90775527898)/(sampleRate * release));
    }
    depth = 1;
    output = 0;
}

EnvelopeFollower::~EnvelopeFollower(){
    
}

void EnvelopeFollower::setParam(float newValue_, int param_){
    if(param_ == attackParam){
        attack = newValue_;
        if(attack <= 0.0){
            atkincr = expf(-float(69.0775527898)/(sampleRate * attack));
        }
        else{
            atkincr = expf(-float(6.90775527898)/(sampleRate * attack));
        }
    }
    else if(param_ == releaseParam){
        release = newValue_;
        if(release <= 0.0){
            relincr = expf(-float(69.0775527898)/(sampleRate * release));
        }
        else{
            relincr = expf(-float(6.90775527898)/(sampleRate * release));
        }
    }
    else if(param_ == depthParam){
        depth = fabs(newValue_);
    }
}

void EnvelopeFollower::setSampleRate(int sampleRate_){
    sampleRate = sampleRate_;
    
    //set atkincr from new sample rate
    if(attack <= 0.0){
        atkincr = expf(-float(69.0775527898)/(sampleRate * attack));
    }
    else{
        atkincr = expf(-float(6.90775527898)/(sampleRate * attack));
    }
    
    //set relincr from new sample rate
    if(release <= 0.0){
        relincr = expf(-float(69.0775527898)/(sampleRate * release));
    }
    else{
        relincr = expf(-float(6.90775527898)/(sampleRate * release));
    }
    
}

float EnvelopeFollower::process(float input_){

    abs = depth * fabs(input_);
    if(output < abs){
        output = abs + atkincr * (output - abs);
    }
    else{
        output = abs + relincr * (output - abs);
    }
    if(output < 0){
        output = 0;
    }
    return output;

}
